<?php

 *
 * Copyright (C) 2008 Thomas Barregren.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
 
 
/**
 * @file
 * MySQL queries for MailServer – a Drupal module that automatically creates,
 * updates and removes e-mail accounts for users with an e-mail domain that the
 * module is configured to manage.
 *
 * Each function below must return a string that can be passed into db_query()
 * as the query argument. The additional arguments that will be passed into
 * db_query() are passed in an array to the function. The function may use
 * these arguments to build the query, or alter them.
 *
 * Author: 
 *   Thomas Barregren <http://drupal.org/user/16678>.
 */


/******************************************************************************
 * MYSQL QUERIES
 ******************************************************************************/

function _mailserver_db_get_domains_query() {
  return <<<SQL
SELECT id, name FROM {mailserver_virtual_domains}
SQL;
}

function _mailserver_db_get_domain_id_query() {
  return <<<SQL
SELECT id FROM {mailserver_virtual_domains} WHERE name = '%s'
SQL;
}

function _mailserver_db_insert_domains_query(&$args) {
  $str = mailserver_token_string(count($args), "('%s')", ',');
  return <<<SQL
INSERT INTO {mailserver_virtual_domains} (name) VALUES $str
SQL;
}

function _mailserver_db_delete_domains_query(&$args) {
  $str = mailserver_token_string(count($args), "'%s'", ',');
  return <<<SQL
DELETE FROM {mailserver_virtual_domains} WHERE name IN ($str)
SQL;
}

function _mailserver_db_insert_users_query($args) {
  $str = mailserver_token_string(count($args), '%s', '|');
  return <<<SQL
INSERT INTO mailserver_virtual_users (id, domain_id, user, password)
SELECT u.uid, d.id, SUBSTRING_INDEX(mail,'@',1), u.pass
FROM users AS u
  INNER JOIN mailserver_virtual_domains AS d
    ON SUBSTRING_INDEX(u.mail,'@',-1) = d.name
WHERE mail REGEXP '.+@($str)'
SQL;
}

function _mailserver_db_insert_user_query() {
  return <<<SQL
INSERT INTO {mailserver_virtual_users} (id, domain_id, user, password) VALUES (%d, %d, '%s', MD5('%s'))
SQL;
}

function _mailserver_db_delete_user_query() {
  return <<<SQL
DELETE FROM {mailserver_virtual_users} WHERE id = %d
SQL;
}

function _mailserver_db_update_user_query() {
  return <<<SQL
UPDATE {mailserver_virtual_users} SET password = MD5('%s') WHERE id = %d
SQL;
}

